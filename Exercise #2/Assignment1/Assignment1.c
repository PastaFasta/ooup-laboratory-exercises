#include<stdio.h>
#include<string.h>

//typedef int (*compar)(const void*, const void*);

const void* mymax(const void* base, size_t nmemb, size_t size, int (*compar)(const void*, const void*)) {
	void* tempBase;
	void* maxElement = (void*)base;
	for (int i = 0; i < nmemb; i++)
	{
		tempBase = (void*)((char*)base + (i)* size);
		if (compar(tempBase, maxElement))
		{
			maxElement = tempBase;
		}
	}
	return maxElement;
}

int gt_int(const void* first, const void* second) {
	if (((int) * (int*)first) > ((int) * (int*)second))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int gt_char(const void* first, const void* second) {
	if (((char) * (char*)first) > ((char) * (char*)second) > 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int gt_str(const void** first, const void** second) {
	if (strcmp(*(char**)first, *(char**)second) > 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


int main() {
	int arr_int[] = { 1, 3, 5, 7, 4, 6, 9, 2, 0 };
	char arr_char[] = "Suncana strana ulice";
	const char* arr_str[] = {
	   "Gle", "malu", "vocku", "poslije", "kise",
	   "Puna", "je", "kapi", "pa", "ih", "njise"
	};
	const int* arrInt = (const int*)mymax(arr_int, sizeof(arr_int) / sizeof(int), sizeof(int), &gt_int);
	printf("%d\n", *arrInt);
	const char* arrChar = (const char*)mymax(arr_char, sizeof(arr_char) / sizeof(char), sizeof(char), &gt_char);
	printf("%c\n", *arrChar);
	const char** arrStr = (const char**)mymax(arr_str, sizeof(arr_str) / sizeof(arrStr), sizeof(arrStr), &gt_str);
	printf("%s", *arrStr);
	return 0;
}