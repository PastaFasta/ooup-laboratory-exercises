def mymax(iterable, key = lambda x: x):
  # incijaliziraj maksimalni element i maksimalni ključ
  max_x=max_key=None
  flag=True

  # obiđi sve elemente
  for x in iterable:
    # ako je key(x) najveći -> ažuriraj max_x i max_key
    if max_key is None or key(x) > max_key:
        max_x = x
        max_key = key(x)

  # vrati rezultat
  return max_x

maxint = mymax([1, 3, 5, 7, 4, 6, 9, 2, 0])
maxchar = mymax("Suncana strana ulice")
maxstring = mymax([
  "Gle", "malu", "vocku", "poslije", "kise",
  "Puna", "je", "kapi", "pa", "ih", "njise"])

print(maxint)
print(maxchar)
print(maxstring)

maxstring = mymax([
  "Gle", "malu", "vocku", "poslije", "kise",
  "Puna", "je", "kapi", "pa", "ih", "njise"], len)
print(maxstring)

D={'burek':8, 'buhtla':5}
maxPrice = mymax(D, D.get)
print(maxPrice)

personas = [("Jon", "Doe"), ("Jim", "Mim"), ("John", "Snow")]
maxPersona = mymax(personas)
print(maxPersona)
