#include<stdio.h>
#include<string.h>
#include <iostream>

template <typename Iterator, typename Predicate>
Iterator mymax(
	Iterator cur, Iterator last, Predicate pred) {
	Iterator maxElement = cur;
	while (cur!=last)
	{
		if (pred(cur, maxElement))
		{
			maxElement = cur;
		}
		cur++;
	}
	return maxElement;
}

int gt_int(const void* first, const void* second) {
	if (((int) * (int*)first) > ((int) * (int*)second))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int gt_char(const void* first, const void* second) {
	if ((((char) * ((char*)first)) > ((char) * ((char*)second))))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int gt_str(const void* first, const void* second) {
	if (strcmp(*(char**)first, *(char**)second) > 0)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

int main() {
	int arr_int[] = { 1, 3, 5, 7, 4, 6, 9, 2, 0 };
	char arr_char[] = "Suncana strana ulice";
	const char* arr_str[] = {
	   "Gle", "malu", "vocku", "poslije", "kise",
	   "Puna", "je", "kapi", "pa", "ih", "njise"
	};
	
	const int* maxint = mymax(&arr_int[0], &arr_int[sizeof(arr_int) / sizeof(*arr_int)], gt_int);
	const char* arrChar = mymax(&arr_char[0], &arr_char[sizeof(arr_char) / sizeof(*arr_char)], gt_char);
	const char** arrStr = mymax(&arr_str[0], &arr_str[sizeof(arr_str) / sizeof(*arr_str)], gt_str);
	
	std::cout << *maxint << "\n";
	printf("%c\n", *arrChar);
	printf("%s", *arrStr);
	return 0;
}