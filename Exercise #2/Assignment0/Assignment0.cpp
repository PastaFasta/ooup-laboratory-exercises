#include <iostream>
#include <assert.h>
#include <stdlib.h>

typedef struct Point {
	int x; int y;
}Point;
typedef struct Shape {
	enum EType { circle, square, rhomb };
	EType type_;
}Shape;
typedef struct Circle {
	Shape::EType type_;
	double radius_;
	Point center_;
}Circle;
typedef struct Square {
	Shape::EType type_;
	double side_;
	Point center_;
}Square;
typedef struct Rhomb {
	Shape::EType type_;
	double side_a_;
	double side_b_;
	double angle_a_;
	double angle_b_;
	Point center_;
};
void drawSquare(Square*) {
	std::cerr << "in drawSquare\n";
}
void drawCircle(Circle*) {
	std::cerr << "in drawCircle\n";
}
void drawRhomb(Rhomb*) {
	std::cerr << "in drawRhomb\n";
}
void drawShapes(Shape** shapes, int n) {
	for (int i = 0; i < n; ++i) {
		Shape* s = shapes[i];
		switch (s->type_) {
		case Shape::square:
			drawSquare((Square*)s);
			break;
		case Shape::circle:
			drawCircle((Circle*)s);
			break;
		case Shape::rhomb:
			drawRhomb((Rhomb*)s);
			break;
		default:
			assert(0);
			exit(0);
		}
	}
}

void moveShapes(Shape** shapes, int n, int x, int y) {
	Shape* shape;
	for (int i = 0; i < n; i++) {
		shape = shapes[i];
		switch (shape->type_) {
		case Shape::square: {
			Square* square = (Square*)shape;
			square->center_.x += x;
			square->center_.y += y;
			break;
		}
		case Shape::circle: {
			Circle* circle = (Circle*)shape;
			circle->center_.x += x;
			circle->center_.y += y;
			break;
		}
		case Shape::rhomb: {
			Rhomb* rhomb = (Rhomb*)shape;
			rhomb->center_.x += x;
			rhomb->center_.y += y;
			break;
		}
		}
	}
}

int main() {
	Shape* shapes[5];
	shapes[0] = (Shape*)new Circle;
	shapes[0]->type_ = Shape::circle;
	shapes[1] = (Shape*)new Square;
	shapes[1]->type_ = Shape::square;
	shapes[2] = (Shape*)new Square;
	shapes[2]->type_ = Shape::square;
	shapes[3] = (Shape*)new Circle;
	shapes[3]->type_ = Shape::circle;
	shapes[4] = (Shape*)new Rhomb;
	shapes[4]->type_ = Shape::rhomb;

	Circle* circle = (Circle*)shapes[0];
	std::cout << circle->center_.x << "\n";
	drawShapes(shapes, 5);
	moveShapes(shapes, 5, 3, 5);
	std::cout << circle->center_.x;
	free(circle);
}