﻿using Assignment4.CalculatePercentiles;
using Assignment4.GenerateNumbers;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment4
{
    class DistributionTester
    {
        private ICalculatePecentile _percCalc;
        private IGenerateNumbers _numGen;

        public DistributionTester(ICalculatePecentile percCalc, IGenerateNumbers numGen)
        {
            _percCalc = percCalc;
            _numGen = numGen;
        }

        public void Test()
        {
            int[] numbers = _numGen.Generate();
            for (int i = 0; i < numbers.Length; i++)
            {
                Console.WriteLine(numbers[i]);
            }
            for (int i = 10; i < 100; i= i+10)
            {
                Console.WriteLine("Percentil {0}: {1}", i, _percCalc.Calculate(i, numbers));
            }
        }
    }
}
