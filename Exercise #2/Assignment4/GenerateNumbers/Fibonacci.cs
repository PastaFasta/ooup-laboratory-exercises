﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment4.GenerateNumbers
{
    class Fibonacci : IGenerateNumbers
    {
        private int _n;

        public Fibonacci(int n)
        {
            _n = n;
        }

        public int[] Generate()
        {
            List<int> NumList = new List<int>();
            int first = 0;
            int second = 1;
            int temp = 0;
            for (int i = 0; i < _n; i++)
            {
                NumList.Add(first);
                temp = first + second;
                first = second;
                second = temp;
            }
            return NumList.ToArray();
        }
    }
}
