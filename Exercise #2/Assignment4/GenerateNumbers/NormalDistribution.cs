﻿using System;
using System.Collections.Generic;

namespace Assignment4.GenerateNumbers
{
    class NormalDistribution : IGenerateNumbers
    {
        private double _mean, _stddev;
        private int _n;
        
        public NormalDistribution(double mean, double stddev, int n)
        {
            _mean = mean;
            _stddev = stddev;
            _n = n;
        }
        public int[] Generate()
        {
            Random rand = new Random(); //reuse this if you are generating many
            List<int> NumList = new List<int>();
            for (int i = 0; i < _n; i++)
            {
                
                double u1 = 1.0 - rand.NextDouble(); //uniform(0,1] random doubles
                double u2 = 1.0 - rand.NextDouble();
                double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                             Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
                double randNormal =
                             _mean + _stddev * randStdNormal; //random normal(mean,stdDev^2)
                NumList.Add((int)randNormal);
            }
            return NumList.ToArray();
        }
    }
}
