﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment4.GenerateNumbers
{
    interface IGenerateNumbers
    {
        int[] Generate();
    }
}
