﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment4.GenerateNumbers
{
    class Sequential : IGenerateNumbers
    {
        private int _from, _to, _step;

        public Sequential(int from, int to, int step)
        {
            _from = from;
            _to = to;
            _step = step;
        }

        public int[] Generate()
        {
            List<int> NumList = new List<int>();
            for (; _from <= _to; _from = _from + _step)
            {
                NumList.Add(_from);
            }
            return NumList.ToArray();
        }
    }
}
