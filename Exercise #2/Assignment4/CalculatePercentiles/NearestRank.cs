﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment4.CalculatePercentiles
{
    class NearestRank : ICalculatePecentile
    {
        public int Calculate(int perc, int[] numbers)
        {
            Array.Sort(numbers);
            int i = (int)(((perc * numbers.Length) / 100) + 0.5);
            return numbers[i];
        }
    }
}
