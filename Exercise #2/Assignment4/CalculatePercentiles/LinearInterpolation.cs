﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment4.CalculatePercentiles
{
    class LinearInterpolation : ICalculatePecentile
    {
        public int Calculate(int perc, int[] numbers)
        {
            for (int i = 0; i < 10; i++)
            {

            }
            Array.Sort(numbers);
            double p_v_i, p_v_i1;
            double interp;
            int N = numbers.Length;

            if (perc < 100 * ((double)1 - 0.5) / N)
                return numbers[0];
            if (perc > 100 * ((double)N - 0.5) / N)
                return numbers[N - 1];
            for (int i = 0; i < N - 1; i++)
            {
                p_v_i = 100 * ((double)i + 1 - 0.5) / N;
                p_v_i1 = 100 * ((double)i + 1 + 1 - 0.5) / N;
                if (perc >= p_v_i && perc <= p_v_i1)
                {
                    interp = numbers[i] + (double)N * (perc - p_v_i) * (numbers[i + 1] - numbers[i]) / 100;
                    return (int)interp+1;
                }
            }
            return new int();
        }
    }
}
