﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment4.CalculatePercentiles
{
    interface ICalculatePecentile
    {
        int Calculate(int perc, int[] numbers);
    }
}
