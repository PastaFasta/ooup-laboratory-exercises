﻿using System;
using Assignment4.CalculatePercentiles;
using Assignment4.GenerateNumbers;

namespace Assignment4
{
    class Program
    {
        static void Main(string[] args)
        {
            //DistributionTester tester1 = new DistributionTester(new NearestRank(), new Fibonacci(12));
            //tester1.Test();
            DistributionTester tester = new DistributionTester(new LinearInterpolation(), new Sequential(1, 12, 1));
            tester.Test();
            
        }
    }
}
