﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Assignment5
{
    class NumberStream : INumberStream
    {
        private List<int> _intColection;
        private INumberSource _numSource;
        private List<Observer> _observers;

        public List<int> GetIntColection()
        {
            return _intColection;
        }

        public NumberStream(INumberSource numSource)
        {
            _numSource = numSource;
            _intColection = new List<int>();
            _observers = new List<Observer>();
        }

        public void Start()
        {
            int number;
            do
            {
                number = _numSource.ReadNumbers();
                Console.WriteLine(number);
                _intColection.Add(number);
                NotifyObserver();
                Thread.Sleep(1000);
            } while (number != -1);
        }

        public void Register(Observer observer)
        {
            _observers.Add(observer);
        }

        public void Unregister(Observer observer)
        {
            _observers.Remove(observer);
        }

        public void NotifyObserver()
        {
            foreach (Observer observer in _observers)
            {
                observer.Update();
            }
        }
    }
}
