﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Assignment5
{
    class FileSource : INumberSource
    {
        private StreamReader _streamReader; 

        public FileSource(string file)
        {
            _streamReader = new StreamReader(file);
        }

        public int ReadNumbers()
        {
            string line;
            int number;
            
            if ((line = _streamReader.ReadLine()) != null)
            {
                try
                {
                    number = int.Parse(line);
                }
                catch
                {

                    return -1;
                }
                if (number > 0)
                {
                    return number;
                }
            }
            return -1;
            
        }
    }
}
