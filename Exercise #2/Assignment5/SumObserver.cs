﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment5
{
    class SumObserver : Observer
    {
        public SumObserver(NumberStream numberStream) : base(numberStream){}
        public override void Update()
        {
            int sum = 0;
            foreach (int number in _numberStream.GetIntColection())
            {
                sum += number;
            }
            Console.WriteLine("Sum is {0}", sum);
        }
    }
}
