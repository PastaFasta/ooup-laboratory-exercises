﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment5
{
    class AvgObserver : Observer
    {
        public AvgObserver(NumberStream numberStream) : base(numberStream) {}
        public override void Update()
        {
            int sum = 0;
            int n = 0;
            foreach (int number in _numberStream.GetIntColection())
            {
                sum += number;
                n++;
            }
            Console.WriteLine("Average is {0}", (double)sum/n);
        }
    }
}
