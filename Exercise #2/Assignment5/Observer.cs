﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment5
{
    abstract class Observer
    {
        protected NumberStream _numberStream;

        protected Observer(NumberStream numberStream)
        {
            _numberStream = numberStream;
            _numberStream.Register(this);
        }

        public abstract void Update();
    }
}
