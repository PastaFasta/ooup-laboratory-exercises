﻿using System;

namespace Assignment5
{
    class Program
    {
        static void Main(string[] args)
        {
            NumberStream numberStream = new NumberStream(new FileSource("nums.txt"));
            FileObserver fileObserver = new FileObserver(numberStream);
            SumObserver sumObserver = new SumObserver(numberStream);
            MedObserver medObserver = new MedObserver(numberStream);
            AvgObserver avgObserver = new AvgObserver(numberStream);
            numberStream.Start();
        }
    }
}
