﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Assignment5
{
    class FileObserver : Observer
    {
        

        public FileObserver(NumberStream numberStrem) : base(numberStrem) {}

        public override void Update()
        {
            using (StreamWriter _streamWriter = new StreamWriter("file.txt"))
            {
                _streamWriter.WriteLine(DateTime.Now.ToString());
                foreach (int number in _numberStream.GetIntColection())
                {
                    _streamWriter.WriteLine(number);
                }
                _streamWriter.Flush();
            }
        }
    }
}
