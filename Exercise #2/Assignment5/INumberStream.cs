﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment5
{
    interface INumberStream
    {
        void Register(Observer observer);

        void Unregister(Observer observer);

        void NotifyObserver();
    }
}
