﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment5
{
    class KeyboardSource : INumberSource
    {
        public int ReadNumbers()
        {
            string line;
            int number;
            Console.WriteLine("Enter number: ");
            if ((line = Console.ReadLine()) != null)
            {
                try
                {
                    number = int.Parse(line);
                }
                catch
                {

                    return -1;
                }
                if (number > 0)
                {
                    return number;
                }
            }
            return -1;
        }
    }
}
