﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assignment5
{
    class MedObserver : Observer
    {
        public MedObserver(NumberStream numberStream) : base(numberStream) {}

        public override void Update()
        {
            List<int> numbers = _numberStream.GetIntColection();
            int numberCount = numbers.Count;
            int halfIndex = numbers.Count / 2;
            numbers.Sort();
            double median;
            if ((numberCount % 2) == 0)
            {
                median = ((numbers.ElementAt(halfIndex) +
                    numbers.ElementAt((halfIndex - 1))) / 2);
            }
            else
            {
                median = numbers.ElementAt(halfIndex);
            }
            Console.WriteLine("Median is {0} ", median);
        }
    }
}
