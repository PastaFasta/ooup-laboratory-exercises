package hr.fer.zemris.ooup.lab2;

import hr.fer.zemris.ooup.lab2.model.Animal;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class AnimalFactory {

    public static Animal newInstance(String animalKind, String animalName){
        try {
            Class<Animal> clazz;
            clazz = (Class<Animal>)Class.forName("hr.fer.zemris.ooup.lab2.plugins." + animalKind);
            Constructor<?> ctr = clazz.getConstructor(String.class);
            Animal animal = (Animal)ctr.newInstance(animalName);
            return animal;
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | InvocationTargetException
                | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }
}
