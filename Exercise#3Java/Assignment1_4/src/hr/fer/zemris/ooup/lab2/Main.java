package hr.fer.zemris.ooup.lab2;

import hr.fer.zemris.ooup.lab2.model.Animal;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        Animal parrot = AnimalFactory.newInstance("Parrot", "Modrobradi");
        Animal tiger = AnimalFactory.newInstance("Tiger", "Straško");

        if (parrot == null || tiger == null)
            return;

        parrot.animalPrintGreeting();
        tiger.animalPrintGreeting();

        parrot.animalPrintMenu();
        tiger.animalPrintMenu();

        List<Animal> animals = generator();
        for(Animal a : animals){
            a.animalPrintGreeting();
            a.animalPrintMenu();
        }
    }

    // -----------------------------------------------------

    private static List<Animal> generator() {
        Random random = new Random();
        List<Animal> list = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            int type = random.nextInt(2);
            Animal p;
            if (type == 1) {
                p = AnimalFactory.newInstance("Parrot", Integer.toString(type));
            } else {
                p = AnimalFactory.newInstance("Tiger", Integer.toString(type));
            }
            list.add(p);
        }
        return list;
    }

}
