#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <locale.h>

typedef char const* (*PTRFUN)();
typedef struct Animal {
		char* name;
		PTRFUN* funcTable;
} Animal;


char const* dogGreet(void){
  return "vau!";
}
char const* dogMenu(void){
  return "kuhanu govedinu";
}
char const* catGreet(void){
  return "mijau!";
}
char const* catMenu(void){
  return "konzerviranu tunjevinu";
}

PTRFUN dogFuncTable[2] = {&dogGreet, &dogMenu};
PTRFUN catFuncTable[2] = {&catGreet, &catMenu};
	

void animalPrintGreeting(Animal* animal){
		printf("%s pozdravlja: %s\n", animal->name, animal->funcTable[0]());
		return;
}

void animalPrintMenu(Animal* animal){
		printf("%s voli: %s\n", animal->name, animal->funcTable[1]());
		return;
}

void constructDog(Animal* Dog, char* name){
	Dog->name = name;
	Dog->funcTable = dogFuncTable;
	return;
}

void constructCat(Animal* Cat, char* name){
	Cat->name = name;
	Cat->funcTable = catFuncTable;
	return;
}

Animal* createDog(char* dogName){
	Animal* dog = (Animal*)malloc(sizeof(Animal));
	constructDog(dog, dogName);
	return dog;
}

Animal* createCat(char* catName){
	Animal* cat = (Animal*)malloc(sizeof(Animal));
	constructCat(cat, catName);
	return cat;
}

void testAnimals(void){
  struct Animal* p1=createDog("Hamlet");
  struct Animal* p2=createCat("Ofelija");
  struct Animal* p3=createDog("Polonije");

  animalPrintGreeting(p1);
  animalPrintGreeting(p2);
  animalPrintGreeting(p3);

  animalPrintMenu(p1);
  animalPrintMenu(p2);
  animalPrintMenu(p3);

  free(p1); free(p2); free(p3);
}

Animal* createDogs(int n){
	Animal* dogs = malloc(n*sizeof(Animal));
	while(n > 0){
		constructDog(dogs + n - 1, "Dog#");
		n--;
	}
	return dogs;
}


int main(void){
	
	Animal* dogs;
	testAnimals();
	dogs = createDogs(10);
	for(int i=0; i<10; i++) printf("%s", dogs[i].name);
	free(dogs);
	return 0;
}