#include <stdio.h>
#include <malloc.h>
#define bool int
#define true 1
#define false 0

typedef double (*PTRFUN)(struct Unary_Funciotn*, double x);
typedef struct Unary_Function{
    int lower_bound;
    int upper_bound;
    PTRFUN* funcTable;
}Unary_Function;

typedef struct Square{
    int lower_bound;
    int upper_bound;
    PTRFUN* funcTable;
}Square;

typedef struct Linear{
    int lower_bound;
    int upper_bound;
    PTRFUN* funcTable;
    double a;
    double b;
}Linear;

PTRFUN unaryFuncTable[2] = {NULL, NULL};
PTRFUN squareFuncTable[2];
PTRFUN lineaerFuncTable[2];

void constructUnary(Unary_Function* unary_function, int* lb, int* ub){
    unary_function->lower_bound = lb;
    unary_function->upper_bound = ub;
    unary_function->funcTable = unaryFuncTable;
    return;
}

Unary_Function* createUnary(int* lb, int* ub){
    Unary_Function* unary_function = malloc(sizeof(Unary_Function));
    constructUnary(unary_function, lb, ub);
    return unary_function;
}

Square* createSquare(int* lb, int* ub){
    Square* square = malloc(sizeof(Square));
    constructUnary(square, lb, ub);
    square->funcTable = squareFuncTable;
    return square;
}

Linear* createLinear(int* lb, int* ub, double a_coef, double b_coef){
    Linear* linear = malloc(sizeof(Linear));
    constructUnary(linear, lb, ub);
    linear->a = a_coef;
    linear->b = b_coef;
    linear->funcTable = lineaerFuncTable;
    return linear;
}

double value_at(Unary_Function* unary_function, double x) {
    return unary_function->funcTable[0](unary_function, x);
}

double negative_value_at(Unary_Function* unary_function, double x){
    double result = value_at(unary_function, x);
    return - result;
}

double squareValueAt(Unary_Function* unary_function, double x){
    return (x*x);
}

double linearValueAt(Linear* linear, double x){
    return ((linear->a) * x + linear->b);
}

void tabulate(Unary_Function* unary_function){
    int x;
    for(x = unary_function->lower_bound; x <= unary_function->upper_bound; x++){
        printf("f(%d)=%lf\n", x, value_at(unary_function, x));
    }
}

static bool same_functions_for_ints(Unary_Function *f1, Unary_Function *f2, double tolerance) {
    int x;
    double delta;

    if(f1->lower_bound != f2->lower_bound) return false;
    if(f1->upper_bound != f2->upper_bound) return false;

    for(x = f1->lower_bound; x <= f1->upper_bound; x++) {
        delta = f1->funcTable[0](f1,x) - f2->funcTable[0](f1,x);
        if(delta < 0){
            delta = -delta;
        }
        if(delta > tolerance){
            return false;
        }
    }
    return true;
}

int main() {
    squareFuncTable[0] = &squareValueAt; squareFuncTable[1] = &negative_value_at;
    lineaerFuncTable[0] = &linearValueAt; lineaerFuncTable[1] = &negative_value_at;
    Unary_Function *f1, *f2;

    f1 = (Unary_Function*) createSquare(-2, 2);
    tabulate(f1);
    f2 = (Unary_Function*) createLinear(-2, 2, 5, -2);
    tabulate(f2);

    printf("f1==f2: %s\n", same_functions_for_ints(f1, f2, 1E-6) ? "DA" : "NE");
    printf("neg_val f2(1) = %lf\n", f2->funcTable[1](f2,1.0));
    free(f1);
    free(f2);

    return 0;
}