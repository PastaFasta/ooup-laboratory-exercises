#include <iostream>

class B{
public:
    virtual int prva()=0;
    virtual int druga()=0;
};

class D: public B{
public:
    virtual int prva(){return 0;}
    virtual int druga(){return 42;}
};

void callWithoutName(B *p){
    int** vtable = *(int***)p;
    int (*funPrva)() = reinterpret_cast<int (*)()>(*vtable);
    int (*funDruga)() = reinterpret_cast<int (*)()>(*(vtable + 1));
    std::cout << (*funPrva)() << std::endl;
    std::cout << (*funDruga)() << std::endl;
}

int main(){
    B *pb = new D;
    callWithoutName(pb);
    return 0;
}