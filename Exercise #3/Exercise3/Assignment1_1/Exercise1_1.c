#include "myfactory.h"
#include <stdio.h>
#include <stdlib.h>

typedef char const* (*PTRFUN)();

typedef struct Animal{
	// vtable entries:
	// 0: char const* name(void* this);
	// 1: char const* greet();
	// 2: char const* menu();
	PTRFUN* funcTable;
}Animal;

// parrots and tigers defined in respective dynamic libraries

// animalPrintGreeting and animalPrintMenu similar as in lab 1
void animalPrintGreeting(Animal* animal) {
	printf("%s pozdravlja: %s\n", animal->funcTable[0](animal), animal->funcTable[1]());
	return;
}

void animalPrintMenu(Animal* animal) {
	printf("%s voli: %s\n", animal->funcTable[0](animal), animal->funcTable[2]());
	return;
}

int main(void) {
	struct Animal* p1 = (struct Animal*)myfactory("./parrot.dll", "Modrobradi");
	struct Animal* p2 = (struct Animal*)myfactory("./tiger.dll", "Stra�ko");
	if (!p1 || !p2) {
		printf("Creation of plug-in objects failed.\n");
		exit(1);
	}

	animalPrintGreeting(p1);//"Sto mu gromova!"
	animalPrintGreeting(p2);//"Mijau!"

	animalPrintMenu(p1);//"brazilske orahe"
	animalPrintMenu(p2);//"mlako mlijeko"

	free(p1); free(p2);
}