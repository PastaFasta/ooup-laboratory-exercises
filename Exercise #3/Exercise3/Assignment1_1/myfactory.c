#include <stdio.h>
#include <windows.h>
#include <winbase.h>
#include <windef.h>
#include "myfactory.h"

typedef struct Animal* (*FUNPTR_AC)(char const*);

void* myfactory(char const* libname, char const* ctorarg)
{
	HMODULE hmodule = LoadLibrary(libname);
	FUNPTR_AC create = (FUNPTR_AC)GetProcAddress(hmodule, "create");
	return create(ctorarg);
}