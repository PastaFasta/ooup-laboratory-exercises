#include <stdio.h>
#include <stdlib.h>

typedef char const* (*PTRFUN)();

typedef struct Parrot {
	PTRFUN* funcTable;
	char const* _name;
} Parrot;

char const* name(void* this) {
	return ((Parrot*)this)->_name;
}

char const* greet() {
	return "Sto mu gromova!";
}

char const* menu() {
	return "brazilske orahe";
}

PTRFUN parrotFuncTable[3] = { (PTRFUN)name, (PTRFUN)greet, (PTRFUN)menu };

void* create(char const* name) {
	Parrot *parrot = (Parrot*)malloc(sizeof(Parrot));
	parrot->funcTable = parrotFuncTable;
	parrot->_name = name;
	return parrot;
}