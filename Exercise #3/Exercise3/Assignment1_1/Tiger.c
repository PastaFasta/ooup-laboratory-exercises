#include <stdio.h>
#include <stdlib.h>

typedef char const* (*PTRFUN)();

typedef struct Tiger {
	PTRFUN* funcTable;
	char const* _name;
} Tiger;

char const* name(void* this) {
	return ((Tiger*)this)->_name;
}

char const* greet() {
	return "Mijauu!";
}

char const* menu() {
	return "mlako mlijeko";
}

PTRFUN tigerFuncTable[3] = { (PTRFUN)name, (PTRFUN)greet, (PTRFUN)menu };

void* create(char const* name) {
	Tiger* tiger = (Tiger*)malloc(sizeof(Tiger));
	tiger->funcTable = tigerFuncTable;
	tiger->_name = name;
	return tiger;
}