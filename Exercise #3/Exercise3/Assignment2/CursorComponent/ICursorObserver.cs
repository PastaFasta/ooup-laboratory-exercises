﻿using Assignment2.LocationComponent;

namespace Assignment2.CursorComponent
{
    public interface ICursorObserver
    {
        void UpdateCursorLocation(Location location);
    }
}