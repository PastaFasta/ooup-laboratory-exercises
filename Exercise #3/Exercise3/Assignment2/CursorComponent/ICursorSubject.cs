﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment2.CursorComponent
{
    interface ICursorSubject
    {
        bool RegisterCursorObserver(ICursorObserver observer);
        bool UnregisterCursorObserver(ICursorObserver observer);
        void NotifyCursorObserver();

        bool MoveCursorLeft(Keys key);
        bool MoveCursorRight(Keys key);
        bool MoveCursorUp();
        bool MoveCursorDown();
    }
}
