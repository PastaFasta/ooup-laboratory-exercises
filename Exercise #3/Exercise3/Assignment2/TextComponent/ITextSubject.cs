﻿using Assignment2.LocationComponent;

namespace Assignment2.TextComponent
{
    interface ITextSubject
    {
        bool RegisterTextObserver(ITextObserver observer);
        bool UnregisterTextObserver(ITextObserver observer);
        void NotifyTextObserver();

        bool DeleteBefore();
        bool DeleteAfter();
        void DeleteRange(LocationRange range);
        LocationRange GetSelectionRange();
        void SetSelectionRange(LocationRange range);
    }
}
