﻿namespace Assignment2.TextComponent
{
    interface ITextObserver
    {
        void UpdateText();
    }
}
