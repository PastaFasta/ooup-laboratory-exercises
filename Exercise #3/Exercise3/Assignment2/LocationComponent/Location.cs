﻿namespace Assignment2.LocationComponent
{
    public class Location
    {
        private int row;
        private int column;
        public int Row { get => row; set => row = value; }
        public int Column { get => column; set => column = value; }

        public Location(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public Location(Location location)
        {
            row = location.row;
            column = location.column;
        }
    }
}
