﻿namespace Assignment2.LocationComponent
{
    class LocationRange
    {
        private Location start;
        private Location end;

        public Location End { get => end; set => end = value; }
        public Location Start { get => start; set => start = value; }

        public LocationRange(Location start, Location end)
        {
            Start = start;
            End = end;
        }
        
        public LocationRange ComparePositions(LocationRange locationRange)
        {
            if (locationRange.Start.Row < locationRange.End.Row)
            {
                return locationRange;
            }
            else if(locationRange.Start.Row > locationRange.End.Row)
            {
                return new LocationRange(locationRange.End, locationRange.Start);
            }
            else
            {
                if (locationRange.Start.Column < locationRange.End.Column)
                {
                    return locationRange;
                }
                else
                {
                    return new LocationRange(locationRange.End, locationRange.Start);
                }
            }
        }
    }
}
