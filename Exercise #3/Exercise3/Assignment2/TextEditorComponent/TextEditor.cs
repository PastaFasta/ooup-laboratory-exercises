﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Assignment2.ClipboardComponent;
using Assignment2.CursorComponent;
using Assignment2.LocationComponent;
using Assignment2.TextComponent;

namespace Assignment2.TextEditorComponent
{
    class TextEditor : Control, ICursorObserver, ITextObserver
    {
        private const int FontSize = 16;
        //private float FontRatio = 0.65f;
        private TextEditorModel _textEditorModel = new TextEditorModel("ioemo \nIDEMOOOOOOO \n tTa put\noko svijeta");
        private Location cursorLocation = new Location(0,0);
        private LocationRange selectedText;
        //private bool selectionExists = false;

        private ClipboardStack clipboardStack = new ClipboardStack();

        public TextEditor()
        {
            this.Focus();
            _textEditorModel.RegisterCursorObserver(this);
            _textEditorModel.RegisterTextObserver(this);
            List<string>.Enumerator enumerator = _textEditorModel.allLines();
        }


        public void UpdateCursorLocation(Location location)
        {
            cursorLocation = location;
            this.Invalidate();
        }

        public void UpdateText()
        {
            Invalidate();
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            int row = 0;
            Font font = new Font(FontFamily.GenericMonospace, FontSize);
            SizeF stringSize = new SizeF();
            
            double width = 0;

            List<string>.Enumerator enumerator = _textEditorModel.allLines();
            while (enumerator.MoveNext())
            {
                stringSize = e.Graphics.MeasureString(enumerator.Current, font);
                width = stringSize.Width / (enumerator.Current.Length+1);
                e.Graphics.DrawString(enumerator.Current, font,
                    new SolidBrush(Color.Black), 0, row);
                row = row + 16;
            }
            
            e.Graphics.DrawString("|", font,
                new SolidBrush(Color.Black), GetCursorX(cursorLocation, width - 1),((cursorLocation.Row)*FontSize));
        }

        private float GetCursorX(Location location, double width)
        {
            if (location.Column == 0)
            {
                //return (FontSize * FontRatio * (-1));
                return - System.Convert.ToSingle(width) / 2;
            }
            else
            {
                return location.Column * System.Convert.ToSingle(width);
            }
        }
        
        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);
            switch (e.KeyCode)
            {
                case Keys.Down:
                    _textEditorModel.MoveCursorDown();
                    Invalidate();
                    break;
                case Keys.Up:
                    _textEditorModel.MoveCursorUp();
                    Invalidate();
                    break;
                case Keys.Left:
                    _textEditorModel.MoveCursorLeft(e.Modifiers);
                    selectedText = _textEditorModel.GetSelectionRange();
                    Invalidate();
                    break;
                case Keys.Right:
                    _textEditorModel.MoveCursorRight(e.Modifiers);
                    selectedText = _textEditorModel.GetSelectionRange();
                    Invalidate();
                    break;
                case Keys.Back:
                    if (selectedText != null)
                    {
                        _textEditorModel.DeleteRange(selectedText);
                    }
                    else
                    {
                        _textEditorModel.DeleteBefore();
                    }
                    selectedText = _textEditorModel.GetSelectionRange();
                    if(_textEditorModel.RearangeLines())
                        _textEditorModel.MoveCursorUp();
                    Invalidate();
                    break;
                case Keys.Delete:
                    if (selectedText != null)
                    {
                        _textEditorModel.DeleteRange(selectedText);
                    }
                    else
                    {
                        _textEditorModel.DeleteAfter();
                    }
                    selectedText = _textEditorModel.GetSelectionRange();
                    if(_textEditorModel.RearangeLines())
                        _textEditorModel.MoveCursorUp();
                    Invalidate();
                    break;
                case Keys.Enter:
                    string line = _textEditorModel.Lines[cursorLocation.Row];
                    _textEditorModel.OnNewLine(line, cursorLocation); 
                    _textEditorModel.MoveCursorDown();
                    Invalidate();
                    break;
                case Keys.Space:
                    _textEditorModel.Insert(Convert.ToChar(" ")); 
                    cursorLocation.Column++;
                    Invalidate();
                    break;
                case Keys.C:
                    if (selectedText != null)
                    {
                        if(e.Modifiers == Keys.Control)
                        {
                            string getline = GetString(selectedText);
                            clipboardStack.PushText(getline);
                        }
                    }
                    break;
                case Keys.X:
                    if (selectedText != null)
                    {
                        clipboardStack.PushText(GetString(selectedText));
                        _textEditorModel.DeleteRange(selectedText);
                    }
                    break;
                case Keys.V:
                    if ((e.Modifiers == Keys.Control))
                    {
                        if(e.Modifiers == Keys.Shift)
                        {
                            _textEditorModel.Insert(clipboardStack.PeekText());
                        }
                        _textEditorModel.Insert(clipboardStack.PopText());
                        
                    }
                    break;
                default:
                    if ((e.KeyValue  >= 0x30 && e.KeyValue <= 0x39) // numbers
                    || (e.KeyValue >= 0x41 && e.KeyValue <= 0x5A) // letters
                    || (e.KeyValue >= 0x60 && e.KeyValue <= 0x69))
                    {
                        _textEditorModel.Insert(Convert.ToChar(e.KeyCode.ToString()));
                        cursorLocation.Column++;
                        Invalidate();
                    }
                    break;
                    
            }
        }

       
        private string GetString(LocationRange locationRange)
        {
            string line;
            locationRange = locationRange.ComparePositions(locationRange);
            int lenght = _textEditorModel.Lines[locationRange.Start.Row].Length - locationRange.End.Column;
            line = _textEditorModel.Lines[locationRange.Start.Row].Substring(locationRange.Start.Column, lenght);
            return line;
        }
    }
}
