﻿using Assignment2.LocationComponent;
using System;
using System.Collections.Generic;
using Assignment2.CursorComponent;
using Assignment2.TextComponent;
using System.Windows.Forms;
using Assignment2.ClipboardComponent;

namespace Assignment2.TextEditorComponent
{
    class TextEditorModel : ICursorSubject, ITextSubject
    {
        private List<string> lines;
        private Location cursorLocation = new Location(0, 0);
        private LocationRange selectionRange = null;

        private List<ICursorObserver> cursorObservers = new List<ICursorObserver>();
        private List<ITextObserver> textObservers = new List<ITextObserver>();


        public List<string> Lines { get => lines; set => lines = value; }
        internal Location CursorLocation { get => cursorLocation; set => cursorLocation = value; }

        public TextEditorModel(string text)
        {
            lines = new List<string>();
            //todo splitaj i po \r
            Lines = new List<string>(text.Split('\n'));
        }

        public List<string>.Enumerator allLines()
        {
            return lines.GetEnumerator();
        }

        public List<string>.Enumerator linesRange(int index1, int index2)
        {
            return lines.GetRange(index1, index2).GetEnumerator();
        }

        //
        //  <CURSOR>
        //
        public bool RegisterCursorObserver(ICursorObserver observer)
        {
            var size = this.cursorObservers.Count;
            this.cursorObservers.Add(observer);
            if (size < this.cursorObservers.Count)
            {
                return true;
            }

            return false;
        }

        public bool UnregisterCursorObserver(ICursorObserver observer)
        {
            return this.cursorObservers.Remove(observer);
        }

        public void NotifyCursorObserver()
        {
            foreach (var observer in cursorObservers)
            {
                observer.UpdateCursorLocation(CursorLocation);
            }
        }

        public bool MoveCursorLeft(Keys key)
        {
            Location start = new Location(cursorLocation);
            if (this.CursorLocation.Column == 0)
            {
                return false;
            }
            if (!(key == Keys.Shift))
            {
                this.CursorLocation.Column -= 1;
                NotifyCursorObserver();
                selectionRange = null;
                return true;
            }
            if (selectionRange == null)
            {
                selectionRange = new LocationRange(start, null);
            }
            this.CursorLocation.Column -= 1;
            selectionRange.End = this.CursorLocation;
            NotifyCursorObserver();
            return true;

        }

        public bool MoveCursorRight(Keys key)
        {
            Location start = new Location(cursorLocation);
            if (this.Lines[this.CursorLocation.Row].Length  == this.CursorLocation.Column)
            {
                return false;
            }
            if (!(key == Keys.Shift))
            {
                this.CursorLocation.Column += 1;
                NotifyCursorObserver();
                selectionRange = null;
                return true;
            }
            if (selectionRange == null)
            {
                    selectionRange = new LocationRange(start, null);
            }
            this.CursorLocation.Column += 1;
            selectionRange.End = cursorLocation;
            NotifyCursorObserver();
            return true;
        }

        public bool MoveCursorUp()
        {
            if (this.CursorLocation.Row == 0)
            {
                return false;
            }
            if (Lines[CursorLocation.Row - 1].Length < CursorLocation.Column)
            {
                this.CursorLocation.Column = Lines[CursorLocation.Row - 1].Length;
                this.CursorLocation.Row -= 1;
            }
            else
            {
                this.CursorLocation.Row -= 1;
            }
            NotifyCursorObserver();
            return true;
        }

        public bool MoveCursorDown()
        {
            if ((this.Lines.Count - 1) == this.CursorLocation.Row)
            {
                return false;
            }
            if (Lines[CursorLocation.Row + 1].Length < CursorLocation.Column)
            {
                this.CursorLocation.Column = Lines[CursorLocation.Row + 1].Length;
                this.CursorLocation.Row += 1;
            }
            else
            {
                this.CursorLocation.Row += 1;
            }
            NotifyCursorObserver();
            return true;
        }   
        
        //
        //  <\CURSOR>
        //

        ///
        /// <TEXT>
        /// 
        public bool RegisterTextObserver(ITextObserver observer)
        {
            var size = this.textObservers.Count;
            this.textObservers.Add(observer);
            if (size < this.textObservers.Count)
            {
                return true;
            }

            return false;
        }

        public bool UnregisterTextObserver(ITextObserver observer)
        {
            return this.textObservers.Remove(observer);
        }

        public void NotifyTextObserver()
        {
            foreach (var observer in textObservers)
            {
                observer.UpdateText();
            }
        }

        public bool DeleteBefore()
        {
            if (cursorLocation.Column == 0)
            {
                //if (Lines[cursorLocation.Row].Length == 0)
                //{
                //    MoveCursorUp();
                //}
                return false;
            }
            else
            {
                Lines[cursorLocation.Row] = Lines[cursorLocation.Row].Remove(cursorLocation.Column-1, 1);
                MoveCursorLeft(Keys.None);
                return true;
            }
        }

        public bool DeleteAfter()
        {
            if (cursorLocation.Column == Lines[cursorLocation.Row].Length)
            {
                return false;
            }
            else
            {
                Lines[cursorLocation.Row] = Lines[cursorLocation.Row].Remove(cursorLocation.Column, 1);
                return true;
            }
        }

        public void DeleteRange(LocationRange range)
        {
            range = range.ComparePositions(range);
            Location curentLocation = range.Start;
            cursorLocation = curentLocation;
            int deltaY = range.End.Row - range.Start.Row - 1;
            if (deltaY < 0)
            {
                Lines[range.Start.Row] = Lines[range.Start.Row].Remove(range.Start.Column, range.End.Column - range.Start.Column);
            }
            else if(deltaY == 0)
            {
                Lines[range.Start.Row] = Lines[range.Start.Row].Remove(range.Start.Column, Lines[range.Start.Row].Length - range.Start.Column);
                Lines[range.End.Row] = Lines[range.End.Row].Remove(0, range.Start.Column);
            }
            
            if (deltaY > 0)
            {
                for (int r = range.Start.Row + 1; r < deltaY; r++)
                {
                Lines[r] = Lines[r].Remove(0, Lines[r].Length);
                }
                Lines[range.End.Row] = Lines[range.End.Row].Remove(0, range.Start.Column);
            }
            selectionRange = null;
            NotifyCursorObserver();
        }

        public LocationRange GetSelectionRange()
        {
            return selectionRange;
        }

        public void SetSelectionRange(LocationRange range)
        {
            selectionRange = range;
        }
        /// 
        /// </TEXT>
        /// 
        public void Insert(char c)
        {
            //if (c == Convert.ToChar("\n"))
            //{

            //}
            string currentLine = Lines[cursorLocation.Row];
            string newChar = Convert.ToString(c);
            string line = currentLine.Substring(0, cursorLocation.Column) +
                newChar.Substring(0,1) + currentLine.Substring(cursorLocation.Column);
            MoveCursorRight(Keys.None);
            Lines[cursorLocation.Row] = line;
        }

        public void Insert(string line)
        {
            foreach (char item in line)
            {
                Insert(item);
            }
        }

        public void OnNewLine(string line, Location cursor)
        {
            List<string> temp = new List<string>();
            
            for (int i = 0; i < cursor.Row; i++)
            {
                temp.Add(Lines[i]);
            }
            temp.Add(line.Substring(0, cursor.Column));
            temp.Add(line.Substring(cursor.Column, line.Length - cursor.Column));
            for (int i = cursor.Row + 1; i < lines.Count; i++)
            {
                temp.Add(Lines[i]);
            }
            Lines = temp;
            NotifyTextObserver();
        }

        public bool RearangeLines()
        {
            List<string> newLines = new List<string>();
            foreach (string item in Lines)
            {
                if(!(item.Length == 0))
                    newLines.Add(item);
            }
            Lines = newLines;
            if (Lines.Count > newLines.Count)
            {
                NotifyTextObserver();
                return true;
            }
            return false;
        }


    }
}
