﻿using System.Collections;
using System.Collections.Generic;

namespace Assignment2.ClipboardComponent
{
    class ClipboardStack : IClipboardSubject
    {
        private Stack<string> stack = new Stack<string>();
        private List<IClipboardObserver> clipboardObservers = new List<IClipboardObserver>();
        
        public void PushText(string line)
        {
            stack.Push(line);
        }

        public string PopText()
        {
            if(IsEmpty())
                return "";
            return stack.Pop();
        }

        public string PeekText()
        {
            if(IsEmpty())
                return "";
            return stack.Peek();
        }

        public bool IsEmpty()
        {
            if(stack.Count == 0)
                return true;
            return false;
        }

        public void ClearStack()
        {
            stack.Clear();
        }

        public bool RegisterClipboardObserver(IClipboardObserver clipboardObserver)
        {
            var size = this.clipboardObservers.Count;
            this.clipboardObservers.Add(clipboardObserver);
            if (size < this.clipboardObservers.Count)
            {
                return true;
            }

            return false;
        }

        public bool UnregisterClipboardObserver(IClipboardObserver clipboardObserver)
        {
            return clipboardObservers.Remove(clipboardObserver);
        }

        public void NotifyClipboardObserver()
        {
            foreach (IClipboardObserver observer in clipboardObservers)
            {
                observer.UpdateClipboard();
            }
        }
    }
}
