﻿namespace Assignment2.ClipboardComponent
{
    interface IClipboardObserver
    {
        void UpdateClipboard();
    }
}
