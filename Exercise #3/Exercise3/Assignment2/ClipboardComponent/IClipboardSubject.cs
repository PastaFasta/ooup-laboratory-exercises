﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment2.ClipboardComponent
{
    interface IClipboardSubject
    {
        bool RegisterClipboardObserver(IClipboardObserver clipboardObserver);
        bool UnregisterClipboardObserver(IClipboardObserver clipboardObserver);
        void NotifyClipboardObserver();
    }
}
