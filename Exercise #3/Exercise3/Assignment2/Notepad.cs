﻿using System.Drawing;
using System.Windows.Forms;

namespace Assignment2.TextEditorComponent
{
    class Notepad : Form
    {
        private TextEditor _textEditor;
        private System.ComponentModel.IContainer components = null;

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this._textEditor = new Assignment2.TextEditorComponent.TextEditor();
            this.SuspendLayout();
            // 
            // _textEditor
            // 
            this._textEditor.Location = new System.Drawing.Point(0, 0);
            this._textEditor.Name = "_textEditor";
            this._textEditor.Size = new System.Drawing.Size(500, 600);
            this._textEditor.TabIndex = 0;
            this._textEditor.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this._textEditor_PreviewKeyDown);
            // 
            // Notepad
            // 
            this.ClientSize = new System.Drawing.Size(500, 600);
            this.Controls.Add(this._textEditor);
            this.Name = "Notepad";
            this.Text = "Notepad";
            this.ResumeLayout(false);

        }
        
        #endregion
        
        public Notepad()
        {
            InitializeComponent();
        }

        private void _textEditor_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Down:
                case Keys.Up:
                case Keys.Left:
                case Keys.Right:
                    e.IsInputKey = true;
                    break;
            }
        }
    }
}
